# Angular-Node API 

Small application making use of Node.js, AngularJS and some external API to load data

## Prepare and run the application
1. I recommend using the latest stable version of Node.js to run this app - Node v6.10.2 as of this moment
2. Once you have node, cd into the application directory and run `$ npm install` to install all dependencies
3. After all dependencies are installed, still being in the application directory run `$ node ./server/server.js` or `$ npm start` to run the application
4. Open your browser and go to `localhost:3000/`