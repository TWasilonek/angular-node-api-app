'use strict';

const express = require("express");
const router = express.Router();

const MyAPI = require("./myAPI/myAPI");
const myAPI = new MyAPI();

router.get('/', (req, res) => {
	// just serve the index file
	res.render('index.html');
});

router.get('/users', (req, res) => {
    myAPI.getUsers()
        .then( (users) => { res.json(users); })
        .catch( (err) => { res.status(404).send(err); });
});

router.get('/users/:id', (req, res) => {
    let id = req.params.id;
   
    myAPI.getUser(id)
        .then( (user) => { res.json(user); })
        .catch( (err) => { res.status(404).send(err); });
});

router.get('/users/:id/todos', (req, res) => {
   let id = req.params.id;
   
    myAPI.getUserTodos(id)
        .then( (user_todos) => { res.json(user_todos); })
        .catch( (err) => { res.status(404).send(err); });
});

router.get('/photos', (req, res) => {
    myAPI.getPhotos()
        .then( (photos) => { res.json(photos); })
        .catch( (err) => { res.status(404).send(err); });
});


module.exports = router;