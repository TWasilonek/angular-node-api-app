/* @name JsonPlaceholderFetcher
 * @description uses jsonplaceholder service to get data in JSON format
*/

'use strict';

const axios = require("axios");

class JsonPlaceholderFetcher {
    constructor () {
       this.rootUrl = 'http://jsonplaceholder.typicode.com';
    }
    
    fetchUsers () {
        return axios.get( this.rootUrl + '/users');
    }
    
    fetchPhotos () {
        return axios.get( this.rootUrl + '/photos');
    }
    
    fetchUserById ( id ) {
        return axios.get( this.rootUrl + '/users/' + id);
    }
    
    fetchUserTodos ( id ) {
        return axios.get( this.rootUrl + '/todos?userId=' + id );
    }
}

module.exports = JsonPlaceholderFetcher;