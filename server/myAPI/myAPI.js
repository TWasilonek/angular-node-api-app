/* @name MyAPI
 * @description Provides a consistent API that drives (and transforms if needed) data from
 * from any data repository to the Router
*/

'use strict';

const JsonPlacehodlerFetcher = require("./jsonPlaceholderFetcher");

class MyAPI {
    constructor() {
        this.jp_fetcher = new JsonPlacehodlerFetcher(); 
    }
    
    _checkIfEmpty( obj ) {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    }
    
    getUsers() {
        return new Promise( (resolve, reject) => {
            console.log('myAPI reached');
            this.jp_fetcher.fetchUsers()
                .then( (users) => { 
                    let empty = this._checkIfEmpty(users.data);
                    
                    if (empty) {
                        throw new Error('404');
                    } else {
                        resolve(users.data);
                    }
                })
                .catch( (err) => {
                    console.log('Error in myAPI.getUsers: ', err);
                    reject( new Error(err));
                });
        });
    }
    
    getPhotos() {
        return new Promise( (resolve, reject) => {
            this.jp_fetcher.fetchPhotos()
                .then( (photos) => { 
                    let empty = this._checkIfEmpty(photos.data);
                    
                    if (empty) {
                        throw new Error('404');
                    } else {
                        resolve(photos.data);
                    }
                })
                .catch( (err) => {
                    console.log('Error in myAPI.getPhotos: ', err);
                    reject( new Error(err));
                });
        });
    }
    
    getUser( id ) {
        return new Promise( (resolve, reject) => {
            this.jp_fetcher.fetchUserById(id)
                .then( (user) => { 
                    let empty = this._checkIfEmpty(user.data);
                    
                    if (empty) {
                        throw new Error('404');
                    } else {
                        resolve(user.data);
                    }
                })
                .catch( (err) => {
                    console.log('Error in myAPI.getUserById: ', err);
                    reject( new Error(err));
                });
        });
    }
    
    getUserTodos( id ) {
        return new Promise( (resolve, reject) => {
            this.jp_fetcher.fetchUserTodos(id)
                .then( (todos) => { 
                    let empty = this._checkIfEmpty(todos.data);
                    
                    if (empty) {
                        throw new Error('404');
                    } else {
                        resolve(todos.data);
                    }
                })
                .catch( (err) => {
                    console.log('Error in myAPI.getUserTodos: ', err);
                    reject( new Error(err));
                });
        });
    }
    
}

module.exports = MyAPI;