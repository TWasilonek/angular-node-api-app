'use strict';

const path = require('path');
const express = require("express");
const compression = require('compression');
const router = require("./router");

const app = express();

/* ---- CONFIG ---- */
const port      = process.env.PORT || '3000';
const hostname  = process.env.IP || '127.0.0.1';

app.use('/', express.static(path.join(__dirname, '../client')));
// app.use(compression());     // Compress response data with gzip


/* ---- ROUTES ---- */
app.use('/', router);

/* ---- START SERVER ----- */
app.listen(port, hostname, () => {
  console.log('Express server listening on port ' + port);
});