/* globals angular */
/**
 * angular-spinner module
 * @module 	app.spinner
 * @description spinner module is responsible for:
 *              - creating and displaying the spinner
 * 
 * Based on Angular-spinner module by Chev - http://codetunnel.io/author/chev/
 * Angular-Spinner:
 *  Github - https://github.com/Chevtek/angular-spinners
 *  Article - http://codetunnel.io/how-to-do-loading-spinners-the-angular-way/
 *  Registering Spinners issue - https://github.com/Chevtek/angular-spinners/issues/4
 * 
 */
(function() {
	'use strict'

	angular.module('angularSpinner', []);

})();