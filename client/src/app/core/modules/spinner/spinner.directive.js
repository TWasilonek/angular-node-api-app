/**
 * App spinner directive
 * @module 	app.spinner
 * @description spinner is responsible for:
 *              - linking the controller to spinner.html
 *              - DOM manipulation of the app.spinner
 */
(function() {
	'use strict';

	angular.module('angularSpinner').directive('spinner', function () {

    return {
      restrict: 'EA',
      transclude: true,
      scope: {
        name: '@?',
        group: '@?',
        show: '=?',
        imgSrc: '@?',
        register: '@?',
        onLoaded: '&?',
        onShow: '&?',
        onHide: '&?'
      },
      templateUrl: './src/app/core/modules/spinner/spinner.html',
      controller: 'SpinnerCtrl',
      controllerAs: 'Spinner'
    };
  });

	
})();

