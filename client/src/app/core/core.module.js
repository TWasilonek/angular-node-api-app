/* global angular */
(function() {
    'use strict';

    angular.module('app.core', [
        /*
         * Angular modules
         */
        'ngRoute',
        'ngAnimate',
        'ngTouch',
        'ui.bootstrap',
        /*
         * Reusable cross app code modules
         */
         'app.templates',
         'angularSpinner'
        /*
         * 3rd Party modules
         */
    ]);
})();
