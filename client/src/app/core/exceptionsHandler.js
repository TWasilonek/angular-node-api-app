/* globals angular, localStorage */

(function() {
    'use strict';
    
    function ExceptionsHandler ($rootScope) {
        this.showErrorModal = showErrorModal;
        
		/* ----- Methods and Properties Assignement ----- */
        function showErrorModal (error) {
            $rootScope.$broadcast('exceptions::exceptionThrown', error);
        }
    }

    ExceptionsHandler['$inject'] = ['$rootScope'];

    angular.module('app.core').service('ExceptionsHandler', ExceptionsHandler);
    
}());