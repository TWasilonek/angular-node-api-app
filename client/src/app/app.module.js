/* global angular */
(function() {
    'use strict';

    angular.module('app', [
        'app.core',
        
        /*
         * Feature areas
         */
        'app.layout',
        'app.users',
        'app.user',
        'app.photos',
        'app.exceptions'
    ]);
    
    angular.module('app').config(['$routeProvider', '$locationProvider', function( $routeProvider, $locationProvider) {
        $locationProvider.html5Mode(false);
        $routeProvider
            .when('/', {
                redirectTo: '/users'
            })
            .when('/users', {
                template: "<users></users>"
            })
            .when('/users/:id', {
                template: "<user></user>"
            })
            .when('/photos', {
                template: "<photos></photos>"
            })
            .otherwise('/');
    }]);

})();