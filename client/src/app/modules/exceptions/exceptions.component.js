/* global angular, $ */
(function(){
    'use strict';
    
    function controller ( $scope, $location ) {
        var vm = this;
        
        vm.handleException = handleException;
        vm.exception = {};
        vm.errorCode = '';
        vm.message = '';
        
        vm.$onInit = function() {
           $scope.$on('exceptions::exceptionThrown', vm.handleException );
           $('#exceptionsModal').on('hide.bs.midal', handleRedirect);
        }
        
        function handleException (event, error) {
            console.log(error);
            if (error.status) {
                vm.errorCode = error.status;
            }
            if ( error.statusText ) {
                vm.message = error.statusText;
            }
            $('#exceptionsModal').modal('show');
        }
        
        function handleRedirect(e) {
            $location.url('/');
        }
        
    }
    
    var exceptions = {
        templateUrl : 'modules/exceptions/exceptions.html',
        controllerAs: 'vm',
        controller: ['$scope', '$location', controller]
    };
    
    angular.module('app.exceptions').component('exceptions', exceptions);
    
})();