/* global angular */
(function(){
    'use strict';
    
    function controller ( $scope, usersModel, usersStorage ) {
        var vm = this;
        
        var model_getUsers = usersModel.getUsers;
        var storage_save = usersStorage.saveToStorage;
        var storage_get = usersStorage.getFromStorage;
        
        vm.users = [];
        vm.usersLength = 0;
        vm.filteredUsers = [];
        vm.loading = true;
        vm.numPerPageChange = numPerPageChange;
        vm.searchName = '';
        vm.sortType     = 'user.name';
        vm.sortReverse  = false;
        
        // ui-bootstrap pagination variables
        $scope.currentPage = 1;
        $scope.numPerPage = 5;
        $scope.maxSize = 5;
        $scope.numPerPageOptions = [
            { id:1, value:3 },
            { id:2, value:5 },
            { id:3, value:10 }
        ];
        
        vm.$onInit = function() {
            model_getUsers()
                .then( function (users) {
                    vm.users = users;
                    storage_save('users_length', users.length);
                    vm.usersLength = storage_get('users_length');
                })
                .then( function () {
                    $scope.$watchCollection('[currentPage, numPerPage]', function() {
                        var begin   = (($scope.currentPage - 1) * $scope.numPerPage);
                        var end     = begin + $scope.numPerPage;
                        vm.filteredUsers = vm.users.slice(begin, end);
                    });
                    vm.loading = false;
                });
        }
        
        
        function numPerPageChange (num) {
            $scope.numPerPage = num;
        }
    }
    
    var users = {
        templateUrl : 'modules/users/users.html',
        controllerAs: 'users',
        controller: ['$scope', 'usersModel', 'usersStorage', controller]
    };
    
    angular.module('app.users').component('users', users);
    
})();