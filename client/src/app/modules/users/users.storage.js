/* globals angular, localStorage */

(function() {
    'use strict';


    function usersStorage () {
        /* ----- Private variables ----- */
        var saveToStorage = saveToStorage;
        var getFromStorage = getFromStorage;

		/* ----- Methods and Properties Assignement ----- */
        function saveToStorage (name, data) {
           localStorage.setItem(name, data);
        }
        
        function getFromStorage (name) {
            return localStorage.getItem(name);
        }

        return {
            saveToStorage : saveToStorage,
            getFromStorage: getFromStorage
        };

    }

    usersStorage['$inject'] = [];

    angular.module('app.users').factory('usersStorage', usersStorage);
    
}());