/* globals angular */

(function() {
    'use strict';


    function usersModel ( $http, ExceptionsHandler ) {
        /* ----- Private variables ----- */
        var getUsers = getUsers;
        
        var exceptions_handle = ExceptionsHandler.showErrorModal;

		/* ----- Methods and Properties Assignement ----- */
        function getUsers () {
            var getUsersUrl = '/users';
            
            console.log('getUsers fires');

            return $http.get(getUsersUrl)
                .then(success)
                .catch(fail)
            
            function success(users) {
                return users.data;
            }
        
            function fail(err) {
                exceptions_handle(err);
            }
        }

        return {
            getUsers : getUsers
        };

    }

    usersModel['$inject'] = ['$http', 'ExceptionsHandler'];

    angular.module('app.users').factory('usersModel', usersModel);
    
}());