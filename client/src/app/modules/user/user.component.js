/* global angular */
(function(){
    'use strict';
    
    function controller ($routeParams, userModel) {
        var vm = this;
            
        var model_getUser = userModel.getUser;
        var model_getTodos = userModel.getUserTodos;
        
        vm.user = {};
        vm.todos = [];
        vm.loading_user = true;
        vm.loadin_todos = true;
        
        vm.$onInit = function() {
            var user_id = $routeParams.id;
            
            model_getUser(user_id)
                .then( function (user) {
                    vm.user = user;
                    vm.loading_user = false;
                });
                
            model_getTodos(user_id)
                .then( function (todos) {
                    vm.todos = todos;
                    vm.loadin_todos = false;
                });
        }
    }
    
    var user = {
        templateUrl : 'modules/user/user.html',
        controllerAs: 'vm',
        controller: ['$routeParams', 'userModel', controller]
    };
    
    angular.module('app.user').component('user', user);
    
})();