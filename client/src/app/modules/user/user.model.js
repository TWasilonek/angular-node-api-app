/* globals angular */

(function() {
    'use strict';


    function userModel ( $http, ExceptionsHandler) {
        /* ----- Private variables ----- */
        var getUser = getUser;
        var getUserTodos = getUserTodos;
        
        var exceptions_handle = ExceptionsHandler.showErrorModal;
        
		/* ----- Methods and Properties Assignement ----- */
        function getUser (id) {
            var getUserUrl = '/users/' + id;

            return $http.get(getUserUrl)
                .then(success)
                .catch(fail)
            
            function success(user) {
                return user.data;
            }
        
            function fail(err) {
                exceptions_handle(err);
            }
        }
        
        
        function getUserTodos (id) {
            var getUserTodosUrl = '/users/' + id + '/todos';

            return $http.get(getUserTodosUrl)
                .then(success)
                .catch(fail)
            
            function success(todos) {
                return todos.data;
            }
        
            function fail(err) {
                exceptions_handle(err);
            }
        }

        return {
            getUser         : getUser,
            getUserTodos    : getUserTodos
        };

    }

    userModel['$inject'] = ['$http', 'ExceptionsHandler'];

    angular.module('app.user').factory('userModel', userModel);
    
}());