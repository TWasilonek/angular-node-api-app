/* global angular */
(function(){
    'use strict';
    
    function controller ( photosModel ) {
        var vm = this;
        
        var model_getPhotos = photosModel.getPhotos;
        
        vm.photos = [];
        vm.loading = true;
        
        vm.$onInit = function() {
            model_getPhotos()
                .then( function (photos) {
                    var i, max = 30;
                    
                    for (i = 0; i < max; i++ ) {
                        vm.photos.push(photos[i]);
                    }
                    
                    vm.loading = false;
                });
        }
    }
    
    var photos = {
        templateUrl : 'modules/photos/photos.html',
        controllerAs: 'vm',
        controller: ['photosModel', controller]
    };
    
    angular.module('app.photos').component('photos', photos);
    
})();