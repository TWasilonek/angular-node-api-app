/* globals angular */

(function() {
    'use strict';


    function photosModel ( $http, ExceptionsHandler ) {
        /* ----- Private variables ----- */
        var getPhotos = getPhotos;
        
        var exceptions_handle = ExceptionsHandler.showErrorModal;

		/* ----- Methods and Properties Assignement ----- */
        function getPhotos () {
            var getPhotosUrl = '/photos';
            
            // console.log('getPhotos fires');

            return $http.get(getPhotosUrl)
                .then(success)
                .catch(fail)
            
            function success(photos) {
                return photos.data;
            }
        
            function fail(err) {
               exceptions_handle(err);
            }
        }

        return {
            getPhotos : getPhotos
        };

    }

    photosModel['$inject'] = ['$http', 'ExceptionsHandler'];

    angular.module('app.photos').factory('photosModel', photosModel);
    
}());