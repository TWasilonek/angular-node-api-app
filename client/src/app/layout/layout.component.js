/* global angular */
(function(){
    'use strict';
    
    var layout = {
        templateUrl : 'layout/layout.html'
    };
    
    angular.module('app.layout').component('layout', layout);
    
})();