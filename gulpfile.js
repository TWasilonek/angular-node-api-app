var gulp = require('gulp');
var del = require('del');
var plumber = require('gulp-plumber');
var beeper = require('beeper');

/* boundling, minification and sourcemaps files */
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate')
var sourcemaps = require('gulp-sourcemaps');
var processhtml = require('gulp-processhtml');
var cleanCSS = require('gulp-clean-css');
var templateCache = require('gulp-angular-templatecache');
var replace = require('gulp-string-replace');

/* PostCss plugins */
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var precss = require('precss');



// Error Helper
function onError(err) {
  beeper();
  console.log(err);
}

// process html task
// gulp.task('processHtml', function () {
//     return gulp.src('client/index.html')
//       .pipe(processhtml())
//       .pipe( gulp.dest('./client/dist')); 
// });

// add all html partials to $templateCache
gulp.task('templateCache', function () {
  return gulp.src([
      './client/src/app/**/*.html'
    ])
      .pipe(templateCache('templates.templateCache.js',{
        module: 'app.templates'
      }))
      .pipe(gulp.dest('./client/src/app/core/modules'));
});

// vendor scripts
gulp.task('vendorJs', function () {
  return gulp.src([
    './client/src/assets/vendor/jquery/jquery.min.js',
    './client/src/assets/vendor/angular/angular.min.js',
    './client/src/assets/vendor/angular/angular-route.min.js',
    './client/src/assets/vendor/angular/angular-animate.min.js',
    './client/src/assets/vendor/angular/angular-touch.min.js',
    './client/src/assets/vendor/angular/ui-bootstrap-tpls.min.js',
    './client/src/assets/vendor/bootstrap/bootstrap.min.js',
    ])
      .pipe(ngAnnotate())
      .pipe(concat('vendor.js'))
      .pipe(uglify())
      .pipe( gulp.dest('./client/dist/scripts'));
});

// vendor css
gulp.task('vendorCss', function(){
  var vendorCssFiles = [
    './client/src/assets/vendor/bootstrap/font-awesome.min.css',
    './client/src/assets/vendor/bootstrap/bootstrap.min.css'
  ];

  return gulp.src( vendorCssFiles )
    .pipe(concat('vendor.css'))
    .pipe(cleanCSS())
    .pipe(gulp.dest('./client/dist/styles'));
});

// process custom css with PostCSS
gulp.task('processCss', function () {
  var processors = [
    precss,
    autoprefixer({browsers: ['last 2 versions', 'ie 9']})
  ];
  return gulp.src('./client/src/app/app.styles.css')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(postcss(processors))
    .pipe(cleanCSS())
    .pipe(gulp.dest('./client/dist/styles'))
});

// processJs task
gulp.task('processJs', function(){
  return gulp.src([
    './client/src/app/app.module.js',
    './client/src/app/**/*.module.js',
    './client/src/app/**/*.js',
    './client/src/app/**/**/*.js'
    ])
      .pipe( replace( /\.\/src\/app\//g, ''))
      .pipe(ngAnnotate())
      .pipe(concat('app.js'))
      .pipe(uglify())
      .pipe( gulp.dest('./client/dist/scripts'));
});

// clean dist task
gulp.task('clean', function(){
    return del([
      './client/dist/**'
    ]);
});

// watcher
gulp.task('watch', function(){
    gulp.watch([
        './client/src/app/**/*.html'
      ], gulp.series('templateCache'));
    gulp.watch([ 
        './client/src/assets/vendor/**/*.css'
      ], gulp.series('vendorCss'));
    gulp.watch([ 
        './client/src/app/*/**.css'
      ], gulp.series('processCss'));
    gulp.watch([
      './client/src/assets/vendor/**/*.js',
      ], gulp.series('vendorJs'));
    gulp.watch([
        './client/src/app/**/*.js',
        './client/src/app/**/**/*.js'
      ], 
        gulp.series('processJs'));
    // gulp.watch('./src/*.html', gulp.series('html'));
});

// default task
gulp.task('default', gulp.series( 
    'clean',
    'templateCache',
    'vendorJs', 
    'vendorCss', 
    'processJs',
    'processCss',
    // 'processHtml',
    'watch'
  )
);


